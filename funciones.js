//funcion declarativa 

function mifuncion1(){
    console.log(3);
}
//llamar la función
mifuncion1();

// expresion

let mifuncion2 = function(a, b){
    console.log(a + b); // suma con riesgo de concatenacion
    console.log(`${a}${b}`); // concatenacion si o si es6
}

//llamar la función
mifuncion2(3, 2);

function saludar(nombre, apellido){
    console.log("hola " + nombre + " " + apellido); //concatenación básica
    console.log(`hola ${nombre} ${apellido}`); // concatenación es6
}
//llamar la función
saludar("Juan", "Vejarano");
saludar("Camilo", "Vejarano");
saludar("Diego", "Ortiz");


//return
function sumar(a, b){
    return (a + b); //retorna el valor de la suma, pero no la imprime
}

function dividir(r){
    return r/2; // retorna la división entre 2
}

//llamar la función
let resultado = sumar(5, 5); // guarda el resultado de la suma
let promedio = dividir(resultado); // guarda el resultado del promedio

console.log(promedio); // imprime el resultado del promedio
