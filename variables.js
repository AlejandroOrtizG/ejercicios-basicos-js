var nombre = "Diego"; // es la manera para declarar una variable sin especificar el tipo de dato. Es recomendable usar "let" en vez de "var"


var apellido; // variable declarada
// debugger
apellido = "Ortiz"; // variable inicializada 
apellido = 123;

let elementos = ["computadora", "celular"]; //esto es un arreglo

let persona = { // esto es una lista
    nombre: "Diego",
    edad: 24

}

let maspersonas = { // esto es una lista con arreglos y valores y más listas
    persona1:{ 
        nombre: "Juan",
        estudios: { // esto es una lista
            estudio1: "Tecnologo",
            estudio2: "Diplomado"
        },
        coloresfavoritos: ["rojo", "amarillo", "azul"] // esto es un arreglo
    },
    persona2:{
        nombre: "Diego",
        estudios: {
            estudio1: "Ingenieria"
        }, 
        coloresfavoritos: ["rojo","azul"]
    }
}