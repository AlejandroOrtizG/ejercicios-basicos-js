let nombre = "Diego"; //scope global
function f(){
    let apellido = "Ortiz"; //scope local
    console.log(`${nombre} ${apellido}`);
    //debugger
    for (let i = 0; i < 3; i++) {
        console.log(i); //scope local
    }
};

f();
